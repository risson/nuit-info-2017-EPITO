<?php
require "Request/connect.php";
if (!$connected)
{
	header("location: index.php");
	exit();
}

$user = $_SESSION["user"];
$ID_user = $user["ID_user"];
$req_data = $db->prepare("SELECT * FROM user WHERE ID = ?");
$req_data->execute(array($ID_user));
$data = $req_data->fetch();

$name = $data["name"];
$firstname = $data["firstname"];
$mail = $data["mail"];
$birthday = $data["birthday"];
$sexe = $data["sexe"];
$weight = $data["weight"];
$register = $data["register"];

$req_histo = $db->prepare("SELECT * FROM alcoolemie WHERE ID_user= ?");
$req_histo->execute(array($ID_user));



require "layout/header.php";
?>

    <h2> Mon Compte </h2>
	<br/>
	<p>Nom : <?= $name ?></p>
	<p>Prénom : <?= $firstname ?></p>
	<p>Mail: <?= $mail ?></p>
	<p>Anniversaire : <?= $birthday ?></p>
	<p>Sexe : <?= $sexe ?></p>
	<p>Poids : <?= $weight ?></p>
	<p>Date d'inscription : <?= $register ?></p>
	<br/>
	<h3> Historique </h3>
	<br/>
	<div>
	<?php
	while ( $data_histo = $req_histo->fetch()) {

		$histo = unserialize($data_histo["data"]);
		echo "<p>  ".$histo["Taux"]." g/L le ".date('d/m/Y', $histo["date"])."</p>";
	}
	?>
	</div>
	
	

<?php
require "layout/footer.php";