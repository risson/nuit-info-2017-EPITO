
<?php
require "Request/connect.php";

require "functions/alcoolemie-calcul.php";
if(isset($_POST["submitalcool"]) && $connected) {
	if (isset($_POST["alcool"]) && isset($_POST["tailleverre"]) && isset($_POST["nbrverres"])) {
		if ((!empty($_POST["alcool"])) && (!empty($_POST["tailleverre"])) && (!empty($_POST["nbrverres"]))) {
			$alcool = $_POST["alcool"];
			$tailleverre = $_POST["tailleverre"];
			$nbrverres = $_POST["nbrverres"];
			$user = $_SESSION["user"];
			$ID_user = $user["ID_user"];
			$req_data = $db->prepare("SELECT * FROM user WHERE ID = ?");
			$req_data->execute(array($ID_user));
			$data = $req_data->fetch();
			$sexe = $data["sexe"];
			$sexe = $sexe == "Homme" ? "H" : "F";
			$weight = $data["weight"];
			$Quantity = CalcQtAlc($nbrverres, $tailleverre, $alcool);
			$TauxAlc = TauxAlc($sexe, $weight, $Quantity);
			$histo =  array("Taux" => $TauxAlc, "date" => time());
			$req_histo = $db->prepare("INSERT INTO alcoolemie (ID_user , data) VALUES (?, ?)");
			$req_histo->execute(array($ID_user, serialize($histo)));
			
			$show_result = true;
		} else {
			
		}
	} else {
		
	}
}

require "layout/header.php";
?>
 <?php if ($connected && !isset($show_result)) {?>
 <h2 class="center">Comment vous sentez-vous ?</h2>
 <div class="divider"></div>
 <div id="result" class="row center">
	 <form idmethod="post" id="alcbien" action="alcoolemie-bien.php">
	 <div class="input-field col s5">
	 <input class="waves-effect waves-light btn green" type="submit" value="Je me sens bien"/>
	 </div>
	 </form>
	 <form method="post" id="alcbour" action="alcoolemie-bourre.php">
	 <div class="input-field col s5 offset-s2">
	 <input class="waves-effect waves-light btn red" type="submit" value="Je me sens bourré"/>
	 </div>
	</form>
 </div>
 <?php } else if ($connected && isset($show_result) && $show_result) { ?>
	<div id="show_result" class="row center">
	<?php 
	echo '<br/>Votre taux d\'alcool est de '.$TauxAlc." g\\L<br/>";

	if($TauxAlc < 0.5)
	{
		echo 'Vous êtes légalement autorisé à conduire.<br/>';
		if($TauxAlc > 0.1)
		{
			echo 'Nous vous conseillons néanmoins de vous faire raccompagner ou de dormir sur place.<br/>';
		}
	}
	else if ($TauxAlc >= 0.5 && $TauxAlc < 0.8)
	{
		echo 'Il serait illégal de conduire, et vous metteriez en danger votre vie et celles d\'innocents. Faites vous raccompagner ou dormez sur place.<br/>';
	}
	else if ($TauxAlc >= 0.8 && $TauxAlc < 1.5)
	{
		echo 'Conduire serait un délit ! Vous metteriez en danger votre vie et celles d\'innocents. Faites vous raccompagner ou dormez sur place.<br/>';
	}
	else
	{
		echo 'Nous sommes surpris que vous arrivez encore à utiliser le site. Sérieusement, dormez sur place. Avec une bassine.<br/>';
	}
	?>
	<br/>
	</div>
 <?php } else { ?>
	<div class="row center">
	Veuillez vous <a href="connect.php">connecter</a> à votre compte pour accéder aux tests.
	<br/>
	<p>Pas encore de compte ?<a href="register.php">Incrivez-vous!</a></p>
 <?php } ?>

<script>

var frm = $('#alcbien');
var frm1 = $('#alcbour');
var res = $('#result');

    frm.submit(function (e) {

        e.preventDefault();

        $.ajax({
            type: frm.attr('method'),
            url: frm.attr('action'),
            data: frm.serialize(),
            success: function (data) {
                console.log('Submission was successful.');
                console.log(data);
				res.html('<p style="font-size: 2em; color: reds" >'+data+"</p>");
            },
            error: function (data) {
                console.log('An error occurred.');
                console.log(data);
            },
        });
    });
	
	frm1.submit(function (e) {

        e.preventDefault();

        $.ajax({
            type: frm1.attr('method'),
            url: frm1.attr('action'),
            data: frm1.serialize(),
            success: function (data) {
                console.log('Submission was successful.');
                console.log(data);
				res.html('<p style="font-size: 2em; color: reds" >'+data+"</p>");
            },
            error: function (data) {
                console.log('An error occurred.');
                console.log(data);
            },
        });
    });

</script>	
<?php
require "layout/footer.php";
