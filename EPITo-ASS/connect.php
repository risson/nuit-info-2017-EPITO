<?php
require "Request/connect.php";

if ($connected)
{
	header("location: index.php");
	exit();
}

if (isset($_POST["submitconnect"])) {
	if ((isset($_POST["pass"])) && (isset($_POST["mail"]))) {
		if ((!empty($_POST["pass"])) && (!empty($_POST["mail"])))
		{
			$mail = $_POST["mail"];
			$pass = hash("sha512",$_POST["pass"]);
			$req_connect = $db->prepare("SELECT * FROM user WHERE mail = ? AND pass = ?");
			$req_connect->execute(array($mail, $pass));
			$exist = ($req_connect->rowCount()) == 1;
			if ($exist)
			{
				$data = $req_connect->fetch();
				$ID_user = $data["ID"];
				$name = $data["name"];
				$firstname = $data["firstname"];
				$token = sha1(time().$name.$_SERVER["REMOTE_ADDR"].$ID_user);
			    $user = array("name" => $name , "firstname" => $firstname, "ID_user" => $ID_user, "_auth" => $token);
			    $_SESSION["user"] = $user;
			    setcookie('_auth',$token,time() + 7200, '/', null, false, true);
			    header("Location: /");
			    exit();
			} else {
				$error = "Mot de passe ou email invalide !";
			}
		} else {
			$error = "Veuillez remplir les champs";
		}
	}
}

require "layout/header.php";
?>

	<h2 class="center">Se connecter</h2>
	<div class="divider"></div>
</br></br>
	<div class="row">
	<div class="col s6 offset-s3">
	<form action="connect.php" method="post">
	<div>
		<label for="mail">Votre Email</label>
		<input type="email" name="mail" id="mail"/>
		<br/>
		<br/>
	</div>
	<div>
		<label for="mdp">Votre mot de passe</label>
		<input type="password" name="pass" id="mdp"/>
		<br/>
		<br/>
	</div>

		<br/>

	<input class="waves-effect waves-light btn" type="submit" name="submitconnect" value="Connexion"/>
	</form>
	</div>
<br/>
	         <?php if (isset($error)) { ?>
         <div class="error card-panel red darken-1">
           <?= $error; ?>
         </div>
         <?php } ?>
	<br/>
	<br/>
<?php
require "layout/footer.php";
