


<br/>
<form method="post" action="alcoolemie.php">
    <p>
		<div class="col s12">
		<br/>
			<label for="alcool">Qu'avez vous bu ?</label><br/>
			<select name="alcool" id="alcool" class="browser-default">
				<optgroup label="Whisky">
					<option value="43">Jack Daniel's</option>
					<option value="40">Grant's</option>
					<option value="40">J&B's</option>
					<option value="40">Label5</option>
				</optgroup>
				<optgroup label="Vodka">
					<option value="37.5">Poliakov</option>
					<option value="37.5">Smirnoff</option>
					<option value="37.5">Eristoff</option>
					<option value="37.5">Sobieski</option>
					<option value="17">Trojka</option>
					<option value="40">Absolut</option>
					<option value="40">Grey Goose</option>
				</optgroup>
				<optgroup label="Bière">
					<option value="4.2">Kronenbourg</option>
					<option value="5">1664</option>
					<option value="5">Heineken</option>
					<option value="5">Hooegarden</option>
					<option value="6">Skoll</option>
					<option value="6">Desperados</option>
					<option value="6.7">Grimbergen Blonde</option>
					<option value="6.5">Grimbergen Brune</option>
					<option value="5">Leffe Ruby</option>
					<option value="6.5">Leffe Blonde</option>
					<option value="7.5">Leffe Royale</option>
					<option value="9">Leffe Rituel</option>
					<option value="8.5">Leffe Trible</option>
				</optgroup>
				<optgroup label="Tequila">
					<option value="43">Tek paf</option>
					<option value="45">Pur</option>
				</optgroup>
				<optgroup label="Rhum">
					<option value="40">Rhum</option>
				</optgroup>
				<optgroup label="Jagermeister">
					<option value="11.3">JagerBomb</option>
					<option value="35">Pur</option>
				</optgroup>
				<optgroup label="Vin">
					<option value="12">Vin</option>
				</optgroup>
				<optgroup label="Champagne">
					<option value="14">Champagne</option>
				</optgroup>
				<optgroup label="Get 27/31">
					<option value="21">Get 27</option>
					<option value="24">Get 31</option>
				</optgroup>
				<optgroup label="Passoa">
					<option value="17">Passoa</option>
				</optgroup>
				<optgroup label="Malibu">
					<option value="18">Malibu</option>
				</optgroup>
			</select>
		</div>
		
		<fieldset>
		<p>Quelle taille de verre ?</p>
		<img src="img/image25cl.jpg" alt="Verre 25cl"/>
		<input type="radio" name="tailleverre" value="25" id="25cl"/> <label for="25cl">Verre 25cl</label>
		<br/>
		<img src="img/imagePinte.jpg" name="tailleverre" alt="Pinte"/>
		<input type="radio" name="tailleverre" value="50" id="pinte"/> <label for="pinte">Pinte (50cl)</label>
		<br/>
		<img src="img/imageShot.jpg" alt="Shot"/>
		<input type="radio" name="tailleverre"value="3" id="shot"/> <label for="shot">Shot</label>
		<br/>
		<img src="img/imageverreavin.jpg" alt="Verre à vin (12cl)"/>
		<input type="radio" name="tailleverre" value="12" id="vin"/> <label for="vin">Verre à vin (12cl)</label>
		<br/>
		
		<label for="nbrverres">Combien de verres ?</label>
		<input type="number" id="nbrverres" name="nbrverres" value="1" />
		
		</fieldset>
		
    </p>
	<input type="submit" value="Envoyer" name="submitalcool">
</form>