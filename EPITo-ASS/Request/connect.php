<?php
date_default_timezone_set("Europe/Paris");

$host_db = "localhost";
$db_name = "nuit2k17";
$login_db = "root";
$password_db = "root";

$db = new PDO('mysql:host='.$host_db.';dbname='.$db_name, $login_db, $password_db); 
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
session_start();
$user = NULL;
$connected = False;
if (isset($_SESSION["user"]) && isset($_COOKIE["_auth"])) {
	$user = $_SESSION["user"];
	if ($user["_auth"] == $_COOKIE["_auth"])
	{
		$user = $_SESSION["user"];
		$connected = True;
	} else {
		$_SESION["user"] = NULL;
		setcookie("_auth", "", time() - 100);
		$user = NULL;
		$connected = False;
	}
} 
