<?php
session_start();
$_SESSION = array();
setcookie("_auth", "", time() - 100);
session_unset();
session_destroy();
session_write_close();
header('Location: index.php');
exit();