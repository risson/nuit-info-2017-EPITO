<?php
require "Request/connect.php";
if ($connected)
{
	header("location: index.php");
	exit();
}


if (isset($_POST["register"])) {
  if ((isset($_POST["name"])) && (isset($_POST["mail"])) && (isset($_POST["pass"])) && (isset($_POST["passcomfirm"])) && (isset($_POST["birthday"])) && (isset($_POST["firstname"])) && (isset($_POST["weight"])) && (isset($_POST["sexe"]))) {
	if ((!empty($_POST["name"])) && (!empty($_POST["mail"])) && (!empty($_POST["pass"])) && (!empty($_POST["passcomfirm"])) && (!empty($_POST["birthday"])) && (!empty($_POST["firstname"])) && (!empty($_POST["weight"])) && (!empty($_POST["sexe"]))) {
		if (filter_var($_POST["mail"], FILTER_VALIDATE_EMAIL)) {
		   $mail = $_POST["mail"];
		   $name = $_POST["name"];
		   $firstname = $_POST["firstname"];
		   $name = $_POST["name"];
		   $pass = hash("sha512",$_POST["pass"]);
		   $passcomfirm = hash("sha512",$_POST["passcomfirm"]);
		   $birthday = $_POST["birthday"];
		   $weight = $_POST["weight"];
		   $sexe = $_POST["sexe"];
		   if ($pass == $passcomfirm)
		   {
			   $req_exist = $db->prepare("SELECT mail FROM user WHERE mail = ?");
			   $req_exist-> execute(array($mail));
			   $free_email = ($req_exist->rowCount()) == 0;
			   if ($free_email)
			   {
				   $req_register = $db->prepare("INSERT INTO user (name, firstname, mail, pass, birthday, register, weight, sexe) VALUES ( ?, ?, ?, ?, ?, NOW(), ?, ?)");
				   $req_register->execute(array($name, $firstname, $mail, $pass, $birthday, $weight ,$sexe));
				   $req_id = $db->prepare("SELECT ID FROM user WHERE mail = ? AND pass = ?");
				   $req_id->execute(array($mail, $pass));
				   $data = ($req_id->fetch());
				   $ID_user = $data["ID"];
				   $token = sha1(time().$name.$_SERVER["REMOTE_ADDR"].$ID_user);
				   $user = array("name" => $name , "firstname" => $firstname, "ID_user" => $ID_user, "_auth" => $token);
				   $_SESSION["user"] = $user;
				   setcookie('_auth',$token,time() + 7200, '/', null, false, true);
				   header("Location: /");
				   exit();
		       } else {
					$error = "Email déjà utlisée !";
			   }
		   } else {
				$error = "Les mots de passe ne correspondent pas.";
		   }
		} else {
			$error = "Email invalide !";
		}
    } else {
		$error = "Veuillez remplir les champs";
	}
  }
}


require "layout/header.php";
?>
	<h2 class="center">Créer un compte</h2>
  <div class="divider"></div>
	<div class="car-panel teal lighten-4">Veuillez entrer vos informations personnelles afin de nous permettre de vous aider à ne pas mourir sur la route.</div>

  <div class="row">
  	<form class="col s12" action= "" method="post">
  		<div class="row">
  			<div class="input-field col s12">
  				<input id="email" type="email" name="mail" class="validate"/>
  				<label class="active" for="email">Email</label>
  			</div>
  		</div>
  		<div class="row">
  			<div class="input-field col s12">
  				<input id="mdp" name="pass" type="password" class="validate"/>
  				<label class="active" for="mdp">Mot de passe</label>
  			</div>
  		</div>
  		<div class="row">
  			<div class="col s12">
  				<input id="passcomfirm" name="passcomfirm" type="password" class="validate"/>
  				<label class="active" for="passconfirm">Confirmez votre mot de passe</label>
  			</div>
  		</div>
  		<div class="row">
  			<div class="input-field col s6">
  				<input id="Prenom" name="firstname" type="text" class="validate"/>
  				<label class="active" for="Prenom">Prénom</label>
  			</div>
  		<div class="input-field col s6">
  			<input id="Nom" name="name" type="text" class="validate"/>
  			<label class="active" for="Nom">Nom</label>
  			</div>
  		</div>
  		<div class="row">
  			<div class="input-field col s12">
  				<input id="Date"name="birthday" type="date" class="validate"/>
  				<label class="active" for="email">Date de naissance</label>
  			</div>
  		</div>
  		<div class="row">
  			<div class="input-field col s12">
  				<input id="poids" name="weight" type="number" class="validate"/>
  				<label class="active" for="poids">Poids</label>
  			</div>
  		</div>
  		<div class="row">
  			<div class="col s12">
  				<p>Sexe</p>
  				<input type="radio" value="Homme" name="sexe" id="H" /> <label for="H">Homme</label>
  				<input type="radio" value="Femme" name="sexe" id="F" /> <label for="F">Femme</label>
  			</div>
  		</div>
      <div class="row">
		<br/>
        <input type="submit" name="register" value="Enregistrer les informations" />
         <br/>
         <br/>
         <?php if (isset($error)) { ?>
         <div class="error card-panel red darken-1">
           <?= $error; ?>
         </div>
         <?php } ?>
      </div>
  	</form>
  </div>
<?php
require "layout/footer.php";
