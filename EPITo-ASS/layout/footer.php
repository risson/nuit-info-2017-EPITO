  </div>

  <!-- Container end -->
    <!--  Scripts-->

    <footer class="page-footer grey lighten-3 blue-grey-text text-darken-4">
      <div class="row">
        <div class="col l6 s12">
          <h5 class="">Conditions d'utilisation</h5>
          <p class="">Nous ne sommes en aucun cas responsables de votre utilisation et de votre interpretation
          de notre prestation. Notre application est uniquement présente à titre informatif et ne remplace en aucun
        cas un ethylotest ou une prise de sang. Nous vous recommandons dans tous les cas de ne pas prendre le volant
      si vous avez consommé de <span class="red-text" >vilaines choses.</span> </p>
        </div>
        <div class="col l4 offset-l2 s12">
          <h5 class="">A voir</h5>
          <ul>
            <li><a class="blue-grey-text text-darken-4" href="#!">Notre Facebook</a></li>
            <li><a class="blue-grey-text text-darken-4" href="#!">Notre Tweeter</a></li>
            <li><a class="blue-grey-text text-darken-4" href="#!">Notre GitHub</a></li>
            <li><a class="blue-grey-text text-darken-4" href="#!">Supportez le projet</a></li>
          </ul>
        </div>
      </div>
    <div class="footer-copyright">
      <div class="container blue-grey-text text-darken-4">
      © 2017 Copyright EPITo
      <a class="blue-grey-text text-darken-4 right" href="#!">Aide</a>
      </div>
    </div>
    </footer>


	<script src="/js/konami.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
    <script>(function($){
  $(function(){
    $('.button-collapse').sideNav();
    $('.parallax').parallax();
    $('.dropdown-button').dropdown();
  })
})(jQuery);</script>
  </body>
</html>