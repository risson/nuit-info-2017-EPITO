<!doctype html>
<html lang="fr">
  <head>
    <title>EPITo-ASS</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
    <link rel="stylesheet" type="text/css"  href="../css/style.css" media="screen,projection"/>
    <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  </head>
  <body>
    <nav class="white" role="navigation">
      <div class="nav-wrapper container">
        <a id="logo-container" href="index.php" class="brand-logo"><img src="img/logo.jpg" width ="50px" height="50px"></a>
        <ul class="right hide-on-med-and-down">
          <li><a href="alcoolemie.php">Alcoolémie</a></li>
		  <li><a href="numeros-urgence.php">Numéro d'urgence</a></li>
		  <?php if ($connected) { ?>
          <li><a href="mon-compte.php">Mon compte</a></li>
		  <li><a href="logout.php">Déconnexion</a></li>
		  <?php } else { ?>
          <li><a href="connect.php">Se connecter</a></li>
		  <li><a href="register.php">S'inscrire</a></li>
		  <?php } ?>
        </ul>

        <ul id="nav-mobile" class="side-nav">
          <li><a href="alcoolemie.php">Alcoolémie</a></li>
		  <li><a href="alcoolemie.php">Alcoolémie</a></li>
          <?php if ($connected) { ?>
          <li><a href="mon-compte.php">Mon compte</a></li>
		  <li><a href="logout.php">Déconnexion</a></li>
		  <?php } else { ?>
          <li><a href="connect.php">Se connecter</a></li>
		  <li><a href="register.php">S'inscrire</a></li>
		  <?php } ?>
        </ul>
        <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
      </div>
    </nav>

  <!-- Navigation bar end -->

  <!-- Parralax banner start -->

    <div id="index-banner" class="parallax-container">
      <div class="section no-pad-bot">
        <div class="container">
          <br><br></br></br></br></br>
          <h1 class="header center white-text">EPITo Alcool Secure System</h1>
        </div>
      </div>
        <div class="parallax"><img src="../img/parallax1.jpg"></div>
    </div>

  <!-- Parallax banner end -->

  <!-- Container start -->

    <div class="container blue-grey-text text-darken-2">
