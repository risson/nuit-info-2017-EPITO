<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="/konami/EPITo/restricted-access/weed.css" />
</head>
<body>
<p id="warning">Si vous avez fumé, ne conduisez pas. La drogue c'est mal.</p>
<title>EPITO-WEED</title>
<h1>Quand avez-vous fumé pour la dernière fois ?</h1>
<figure>
    <img src="logoWeed.jpg" id="logo" alt="Logo Weed"/>
    <figcaption>Version cannabis activée</figcaption>
</figure>
<form id="formweed" method="post" action="weed.php">
    <div>
        <label for="date">Date :</label>
        <input type="date" name="date" id="date"/>
        <br/>
        <br/>
    </div>
    <div>
        <label for="heure">Heure :</label>
        <input type="time" name="heure" id="heure"/>
        <br/>
        <br/>
    </div>
    <input type="submit" name="submit" value="Envoyer"/>
</form>
<br/>
<div id="result"></div>


<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script>

var frm = $('#formweed');
var res = $('#result');

    frm.submit(function (e) {

        e.preventDefault();

        $.ajax({
            type: frm.attr('method'),
            url: frm.attr('action'),
            data: frm.serialize(),
            success: function (data) {
                console.log('Submission was successful.');
                console.log(data);
				res.html('<p style="font-size: 2em; color: reds" >'+data+"</p>");
            },
            error: function (data) {
                console.log('An error occurred.');
                console.log(data);
            },
        });
    });

</script>	
</body>
</html>