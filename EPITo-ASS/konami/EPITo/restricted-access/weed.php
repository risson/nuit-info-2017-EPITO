<?php
date_default_timezone_set("Europe/Paris");
if (isset($_POST['date']) && isset($_POST['heure'])) {
	$date = $_POST['date'];
	$heure = $_POST['heure'];
	$annee = (int)preg_replace("#^([0-9]{4})-([0-9]{2})-([0-9]{2})$#i", "$1", $date);
	$mois = (int)preg_replace("#^([0-9]{4})-([0-9]{2})-([0-9]{2})$#i", "$2", $date);
	$jour = (int)preg_replace("#^([0-9]{4})-([0-9]{2})-([0-9]{2})$#i", "$3", $date);
	$h = (int)preg_replace("#^([0-9]{2}):([0-9]{2})$#i", "$1", $heure);
	$m = (int)preg_replace("#^([0-9]{2}):([0-9]{2})$#i", "$2", $heure);
	$timestamp = mktime($h, $m, 0, $mois, $jour, $annee);
	$a = time();
	if ($timestamp < time()) {
		$tps = time() - $timestamp ;
		if ($tps <= 6 * 60 * 60) {
			echo "Vous êtes positif au test salivaire , urinaire et également capilaire.";
		} else if ($tps <= 21 * 24*60*60) {
			echo "Vous êtes positif au test urinaire et capilaire en revanche pas au test salivaire.";
		} else if ($tps <= 21 * 30*24*60*60) {
			echo "Vous êtes positif au test capilaire mais pas à l'urinaire et au salivaire.";
		} else {
			echo "Vous n'êtes pas dépistable.";
		}
	} else {
		echo "Vous êtes vraiment trop défoncé !";
	}
}