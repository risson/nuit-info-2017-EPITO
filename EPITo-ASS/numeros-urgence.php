<?php
require "Request/connect.php";


require "layout/header.php";
?>

<h1>Numéros d'urgence</h1>

<p>Appelez ces numéros en cas de problème :</p>
<ul>
	<li>Numéro européen : <a href="tel:112">112</a></li>
	<li>Samu : <a href="tel:15">15</a></li>
	<li>Pompiers : <a href="tel:18">18</a></li>
	<li>Police : <a href="tel:17">17</a></li>
<ul>


<?php
require "layout/footer.php";