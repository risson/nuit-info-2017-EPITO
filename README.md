---
author: EPITo
date: 8 décembre 2017
title: Nuit de l’info 2017 - Document de rendu
---

# Nuit de l’info 2017 - Document de rendu

Sujet commun {#sujet-commun .unnumbered}
============

Pour le sujet national, nous avons décidé de nous orienter vers une action de prévention contre la conduite après une soirée trop arrosée. Pour ce faire, nous avons  créé un site web qui permet à l’utilisateur de vérifier s’il a trop bu pour conduire. Ce dernier doit indiquer ce qu’il a bu, son taux d’alcool dans le sang est calculé et on lui indique s’il a le droit de conduire ou non. Les paramètres pris en compte sont son poids, son sexe, s’il est jeune conducteur, sans oublier ce qu’il a bu. L’utilisateur a également la possibilité de se créer un compte, afin de sauvegarder ses dernières soirées et ses informations personnelles pour ne pas avoir à les rentrer à chaque fois. Une page “Numéros d’urgence” est également disponible, ainsi que des messages de prévention parsemés sur le site. Cf. pour l’accès à notre projet.

404Games {#games .unnumbered}
========

Nous avons choisi de faire une page 404 en lien avec le thème de notre
site. Sur un fond de verre pilé (les bouteilles qui tombent en fin de
soirée), un homme essaye d’ouvrir une bouteille de champagne, d’une
manière plutôt originale. Cf. pour l’accès à notre projet.

Je géolocalise, tu géolocalises, nous géolocalisons !! {#je-geolocalise-tu-geolocalises-nous-geolocalisons .unnumbered}
======================================================

Ce défi n’a malheureusement pas pu aboutir ;( En effet, les navigateurs modernes nécessitent le protocole HTTPS afin d’utiliser la géolocalisation. N’ayant pas le temps, ni le budget, de se procurer un certificat SSL afin de réaliser nos tests, ni la connaissance pour éviter ladite restriction des navigateurs, nous avons choisi d’abandonner ce défi.

KONAMI CODE {#konami-code .unnumbered}
===========

Pour ce défi, nous nous sommes attaqués à la consommation d’autre substances que l’alcool, un autre fléau présent sur nos routes. Cf. pour l’accès à notre projet.

Méthode de rendu {#methode-rendu .unnumbered}
================

Notre projet est disponible sur le serveur OVH dans le “home” de l’utilisateur “jdoe” qui nous a été attribué. Il est également disponible sur [GitHub](https://github.com/Marcerisson/EPITO-Nuit-de-l-info-2017). Afin de lancer notre projet, vous aurez besoin d’un serveur web capable d’éxecuter du PHP, ainsi que d’un server MySQL dans lequel il faudra importer la base de données disponible dans `EPITo-ASS/db/nuit2k17.sql`. Notre site sera également disponible d’ici peu pour test [ici](http://epito-ass.marcerisson.space). Pour plus d’informations, le leader de notre groupe reste joignable par [mail](mailto:marc.schmitt@epita.fr).

Améliorations futures {#améliorations-futures .unnumbered}
=====================

- Possibilité de “commencer une soirée” et d’ajouter les verres consommés au fur et à mesure de la soirée, en prenant en compte l’écart de temps entre l’ajout des différents verres, pour un calcul plus précis de l’alcoolémie

- Ajouter des boissons personnalisées par l’utilisateur, qui peuvent devenir, après modération, disponible pour tous les utilisateurs

- Appel d’un taxi si impossibilité de conduire

- Affichage des transports en communs encore disponibles

- Affichage de l’historique des soirées avec un détail de ce que l’utilisateur a bu

Autres notes {#autres-notes .unnumbered}
============

Nous aurions aimé nous inscrire au défi “Mascotte/Logo” (oubli de notre part), mais malgré cela, nous avons tout de même fait un super logo !
